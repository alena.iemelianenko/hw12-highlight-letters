//1.Чому для роботи з input не рекомендується використовувати клавіатуру?
// у зв'язку із тим що у поле input ми можемо не тільки друкувати інформацію, а ще і, наприклад, вставляти. тому краще використовувати спеціальну подію input.


//Завдання:

let buttons = document.querySelectorAll(".btn");

document.addEventListener('keydown', hilightButton);

function hilightButton (e) {
    let eKey = e.key.toUpperCase();
    
    for (let button of buttons) {
        if (button.textContent === eKey || button.textContent === e.key) {
            button.style.backgroundColor = 'blue'
        } else (button.style.backgroundColor = '#000000')
    }
}